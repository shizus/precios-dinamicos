<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main" />
    <title></title>
</head>

<body>
    <g:if test="${offersCount == 0}">
        No hay ofertas disponibles
    </g:if>
    <g:if test="${offersCount > 0}">
        La siguiente oferta esta disponible:
        Se le entregaran los siguientes productos:
        <g:each in="${offers.products}" var="product">
            <div>
                ${product.name}
            </div>
        </g:each>
        por el price:
        <div>
            ${offers.amount}
        </div>
    </g:if>
</body>
</html>