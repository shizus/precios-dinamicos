<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:if test="params.category">
            <title>${params.category}</title>

        </g:if>
        <g:else>        <title>Precios Dinámicos</title>
        </g:else>

    </head>
    <body>
        %{--<a href="#list-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
        %{--<div id="list-product" class="content scaffold-list" role="main">--}%
            %{--<g:if test="${flash.message}">--}%
                %{--<div class="message" role="status">${flash.message}</div>--}%
            %{--</g:if>--}%

            %{--<f:table collection="${productList}" />--}%
            %{--<g:if test="${productCount > max}">--}%
                %{--<div class="pagination">--}%
                    %{--<g:paginate total="${productCount ?: 0}"/>--}%
                %{--</div>--}%
            %{--</g:if>--}%

        %{--</div>--}%


    <div id="page">

        <div id="fh5co-product">
            <div class="container">
                <g:if test="${params.category}">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                            <h2>${params.category}</h2>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                            <span><g:message code="home.cool_stuff"/></span>
                            <h2><g:message code="home.our_products"/></h2>
                            <p><g:message code="home.products_description"/></p>
                        </div>
                    </div>
                </g:else>

                <div class="row">
                    <g:each in="${productList}" var="product" status="i">
                        <div class="col-md-4 text-center">
                            <div class="product">
                                <div class="product-grid" style="background-image:url(/assets/products/${product.id}.jpg);">
                                    <div class="inner">
                                        <p>
                                            %{--<a href="single.html" class="icon"><i class="icon-shopping-cart"></i></a>--}%
                                            <g:link action="show" class="icon" id="${product.id}"><i class="icon-eye"></i></g:link>
                                        </p>
                                    </div>
                                </div>
                                <div class="desc">
                                    <h3><a href="single.html">${product.name}</a></h3>
                                </div>
                            </div>
                        </div>
                        <g:if test="${((i+1) % 3) == 0}">
                            </div>
                            <div class="row">
                        </g:if>
                    </g:each>
            </div>
            </div>
        </div>


        <div id="fh5co-services" class="fh5co-bg-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 text-center">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-credit-card"></i>
                            </span>
                            <h3>Credit Card</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove</p>
                            <p><a href="#" class="btn btn-primary btn-outline">Learn More</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-wallet"></i>
                            </span>
                            <h3>Save Money</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove</p>
                            <p><a href="#" class="btn btn-primary btn-outline">Learn More</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-paper-plane"></i>
                            </span>
                            <h3>Free Delivery</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove</p>
                            <p><a href="#" class="btn btn-primary btn-outline">Learn More</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="fh5co-testimonial" class="fh5co-bg-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <span>Testimony</span>
                        <h2>Happy Clients</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="owl-carousel owl-carousel-fullwidth">
                                <div class="item">
                                    <div class="testimony-slide active text-center">
                                        <figure>
                                            <img src="images/person1.jpg" alt="user">
                                        </figure>
                                        <span>Jean Doe, via <a href="#" class="twitter">Twitter</a></span>
                                        <blockquote>
                                            <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimony-slide active text-center">
                                        <figure>
                                            <img src="images/person2.jpg" alt="user">
                                        </figure>
                                        <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                        <blockquote>
                                            <p>&ldquo;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimony-slide active text-center">
                                        <figure>
                                            <img src="images/person3.jpg" alt="user">
                                        </figure>
                                        <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                        <blockquote>
                                            <p>&ldquo;Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="fh5co-counter" class="fh5co-bg fh5co-counter" style="background-image:url(images/img_bg_5.jpg);">
            <div class="container">
                <div class="row">
                    <div class="display-t">
                        <div class="display-tc">
                            <div class="col-md-3 col-sm-6">
                                <div class="feature-center">
                                    <span class="icon">
                                        <i class="icon-eye"></i>
                                    </span>

                                    <span class="counter js-counter" data-from="0" data-to="22070" data-speed="5000" data-refresh-interval="50">1</span>
                                    <span class="counter-label">Creativity Fuel</span>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="feature-center">
                                    <span class="icon">
                                        <i class="icon-shopping-cart"></i>
                                    </span>

                                    <span class="counter js-counter" data-from="0" data-to="450" data-speed="5000" data-refresh-interval="50">1</span>
                                    <span class="counter-label">Happy Clients</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="feature-center">
                                    <span class="icon">
                                        <i class="icon-shop"></i>
                                    </span>
                                    <span class="counter js-counter" data-from="0" data-to="700" data-speed="5000" data-refresh-interval="50">1</span>
                                    <span class="counter-label">All Products</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="feature-center">
                                    <span class="icon">
                                        <i class="icon-clock"></i>
                                    </span>

                                    <span class="counter js-counter" data-from="0" data-to="5605" data-speed="5000" data-refresh-interval="50">1</span>
                                    <span class="counter-label">Hours Spent</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="fh5co-started">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <h2>Newsletter</h2>
                        <p>Just stay tune for our latest Product. Now you can subscribe</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form class="form-inline">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email</label>
                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <button type="submit" class="btn btn-default btn-block">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>