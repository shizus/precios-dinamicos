<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>${product.name}</title>
    </head>
    <body>

    <div id="page">

        <g:if test="${user && user.isAdmin}">
            <div class="nav" role="navigation">
                <ul>
                    <li><g:link class="create" action="create"><g:message code="product.add"/> </g:link></li>
                    <li><g:link class="create" action="edit" id="${product.id}"><g:message code="default.button.edit.label"/></g:link></li>
                </ul>
            </div>
        </g:if>

        <div id="fh5co-product">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-6 text-center fh5co-heading">
                                <asset:image src="products/${product.id}.jpg" alt="${product.name}" title="${product.name}"/>
                            </div>
                            <div class="col-md-6 text-right fh5co-heading">
                                <h2>${product.name}</h2>
                                <span class="price">$${product.calculateDinamicPrice(user)}</span>
                                <p>${product.description}</p>
                                <p>
                                    <g:link class="btn btn-primary btn-outline btn-lg" action="addToCart" resource="${this.product}"><g:message code="product.add_to_cart" /></g:link>
                                    <g:if test="${isAdmin}">
                                        <g:form resource="${this.product}" method="DELETE">
                                            <g:link class="btn btn-info btn-outline btn-lg" action="edit" resource="${this.product}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                            <input class="btn btn-danger btn-outline btn-lg" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                        </g:form>
                                    </g:if>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
    </div>
    </body>
</html>
