<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'currentDay.label', default: 'CurrentDay')}" />
        <title>Precios Dinámicos</title>
    </head>
    <body>
        <a href="#list-currentDay" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-currentDay" class="content scaffold-list" role="main">
            <h1>Precios Dinámicos</h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${currentDayList}" />

            <div class="pagination">
                <g:paginate total="${currentDayCount ?: 0}" />
            </div>
        </div>
    </body>
</html>