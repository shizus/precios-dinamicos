<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ticket.label', default: 'Ticket')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-ticket" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-ticket" class="content scaffold-show" role="main">
            <h2><g:message code="default.show.label" args="[entityName]" /></h2>
            <f:table collection="${this.ticket.purchase.products}" />
            <g:form resource="${this.ticket}" method="DELETE">
                <fieldset class="buttons">
            <g:link class="create" action="pay" resource="${this.ticket}"><g:message code="default.button.buy.label"
                                                                                           default="buy"/></g:link>
            %{--<g:link class="edit" action="edit" resource="${this.ticket}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        --}%</fieldset>
    </g:form>
</div>
</body>
</html>
