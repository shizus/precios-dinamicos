<%@ page import="preciosdinamicos.CartController" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Precios Dinámicos</title>
</head>

<body>
<div id="page">
    <div id="fh5co-product">
        <div class="container">
            <div id="show-cart" class="row" role="main">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2><g:message code="cart"/></h2>

                </div>

                <table class='display'>
                    <tr>
                        <th><g:message code="cart.image"/></th>
                        <th><g:message code="cart.name"/></th>
                        <th><g:message code="cart.description"/></th>
                        <th><g:message code="cart.price"/></th>
                        <th><g:message code="cart.category"/></th>
                        <th></th>
                    </tr>
                    <g:each in="${this.cart.products}" var="product">
                        <tr>
                            <td>
                                <asset:image src="products/${product.id}.jpg" height="50" alt="${product.name}" title="${product.name}"/>
                            </td>
                            <td>${product.name}</td>
                            <td>${product.description}</td>
                            <td>${product.price.amount}</td>
                            <td>${product.category}</td>
                            <td>
                                <g:form controller="CartController">

                                    <g:link class="delete" action="remove" params="${['id': product.id]}"><g:message
                                            code="default.button.eliminar.label"
                                            default="Borrar"/></g:link>
                                </g:form></td>
                        </tr>
                    </g:each>
                </table>
                <g:form  controller="CartController" >
                    <g:link class="btn btn-primary btn-outline btn-md" action="buy" resource="${this.cart}"><g:message code="default.button.buy.label"
                                                                                                                       default="buy"/></g:link>
                    <g:link class="btn btn-danger btn-outline btn-md" action="clear"><g:message code="default.button.cart.clear.label"
                                                                                                default="clear"/></g:link>
                    <g:link class="btn btn-info btn-outline btn-md" action="goToPurchases"><g:message code="default.button.purchases.label"
                                                                             default="Ir a Compras"/></g:link>
                </g:form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
