<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'productCategory.label', default: 'ProductCategory')}" />
        <title>Precios Dinámicos</title>
    </head>
    <body>
        <a href="#list-productCategory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create">Agregar Nueva Categoría</g:link></li>
            </ul>
        </div>
        <div id="list-productCategory" class="content scaffold-list" role="main">
            <h1>Precios Dinámicos</h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${productCategoryList}" />

            <div class="pagination">
                <g:paginate total="${productCategoryCount ?: 0}" />
            </div>
        </div>
    </body>
</html>