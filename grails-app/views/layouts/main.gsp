<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Precios Dinámicos"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="animate.css"/>
    <asset:stylesheet src="icomoon.css"/>
    <asset:stylesheet src="bootstrap.css"/>
    <asset:stylesheet src="flexslider.css"/>
    <asset:stylesheet src="owl.carousel.min.css"/>
    <asset:stylesheet src="owl.theme.default.min.css"/>
    <asset:stylesheet src="style.css"/>
    <asset:javascript src="modernizr-2.6.2.min.js"/>
    <!--[if lt IE 9]>
    <asset:javascript src="respond.min.js"/>
    <![endif]-->
    <g:layoutHead/>
</head>
<body>
    <%@ page import="preciosdinamicos.CurrentDayController" %>
    <nav class="fh5co-nav" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xs-2">
                    <div id="fh5co-logo"><a href="${createLink(uri: '/')}"><asset:image width="100" src="preciosDinamicosLogo.png"/></a>
                    </div>
                </div>
                <div class="col-md-7 col-xs-6 text-center menu-1">
                    <ul>
                        <li class="has-dropdown">

                            <g:link controller="product"><g:message code="default.store"/></g:link>
                            <ul class="dropdown">
                                <li><g:link controller="product" action="index" params="[category: 'Libros']"><g:message code="default.books"/></g:link></li>
                                <li><g:link controller="product" action="index" params="[category: 'Tazas']"><g:message code="default.cups"/></g:link></li>
                            </ul>
                        </li>

                        <li><a href="about.html"><g:message code="default.about"/></a></li>
                        <li><a href="${createLink(uri: '/offer/show')}"><g:message code="default.button.offers"/></a></li>
                        <li><a href="${createLink(uri: '/contact')}"><g:message code="default.contact"/></a></li>
                        <li>Días <preciosdinamicos:currentDay /></li>
                        <li>                        <my:userSelector name="userSelector"/>
                        </li>

                    </ul>
                </div>
                <div class="col-md-3 col-xs-4 text-right hidden-xs menu-2">
                    <ul>
                        %{--<li class="search">--}%
                            %{--<div class="input-group">--}%
                                    %{--<input type="text" placeholder="Search..">--}%
                                    %{--<span class="input-group-btn">--}%
                                    %{--<button class="btn btn-primary" type="button"><i class="icon-search"></i></button>--}%
                                %{--</span>--}%
                            %{--</div>--}%
                        %{--</li>--}%
                        <g:if test="${session.user}">
                            <li class="shopping-cart"><a title="<g:message code="default.button.goToCart"/>" href="${createLink(uri: '/cart/goToCart')}" class="cart"><span>%{--<small>0</small>--}%<i class="icon-shopping-cart"></i></span></a></li>
                        </g:if>
                    </ul>
                </div>
        </div>

    </div>
</nav>
<div class="fh5co-loader"></div>

<g:if test="${flash.message}">
    <div class="message ${flash.class}" role="status">${flash.message}</div>
</g:if>
    <asset:javascript src="login.js"/>

    <g:layoutBody/>

    <footer id="fh5co-footer" role="contentinfo">
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-4 fh5co-widget">
                    <h3><g:message code="default.site_name" /></h3>
                    <p><g:message code="default.site_footer" /></p>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                    <ul class="fh5co-footer-links">
                        <li><a href="#"><g:message code="default.about" /></a></li>
                        <li><a href="#"><g:message code="default.help" /></a></li>
                        <li><a href="#"><g:message code="default.contact" /></a></li>
                        <li><a href="#"><g:message code="default.terms" /></a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                    <ul class="fh5co-footer-links">
                        <li><a href="#"><g:message code="default.store" /></a></li>
                        <li><a href="#"><g:message code="default.privacy" /></a></li>
                        <li><a href="#"><g:message code="default.testimonials" /></a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                    <ul class="fh5co-footer-links">
                        <li><a href="#"><g:message code="default.team" /></a></li>
                    </ul>
                </div>
            </div>

            <div class="row copyright">
                <div class="col-md-12 text-center">
                    <ul class="fh5co-social-icons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                    </ul>
                    <p>
                        <small class="block">&copy; 2017 FIUBA | <a href="http://creativecommons.org.ar/licencias" target="_blank"><asset:image width="100" src="by-nc-sa-125px.png"/></a> .</small>
                    </p>
                    <p>

                </p>
                </div>
            </div>

        </div>
    </footer>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <asset:javascript src="application.js"/>

</body>
</html>
