<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'purchase.label', default: 'Purchase')}" />
        <title>Compras</title>
    </head>
    <body>
        <a href="#list-purchase" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-purchase" class="content scaffold-list" role="main">
            <h1>Compras</h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <table class='display'>
                <tr>
                    <th><g:message code="purchase.date"/></th>
                    <th><g:message code="purchase.amount"/></th>
                    <th><g:message code="purchase.products"/></th>
                    <th></th>
                </tr>
                <g:each in="${purchaseList}" var="purchase">
                    <tr>
                        <td>${purchase.date}</td>
                        <td>${purchase.amount}</td>
                        <td>${purchase.products*.name.join(",")}</td>
                    </tr>
                </g:each>
            </table>
            <g:form  controller="PurchaseController" >
                <fieldset class="buttons">
                    <g:link class="delete" action="removeInvalidPurchases"><g:message code="default.button.purchase.clear.label"
                                                                     default="clear"/></g:link>
                </fieldset>
            </g:form>
        </div>
    </body>
</html>