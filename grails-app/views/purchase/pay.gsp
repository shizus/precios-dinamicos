<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'purchase', default: 'Purchase')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
    <div id="page">

        <g:if test="${user && user.isAdmin}">
            <div class="nav" role="navigation">
                <ul>
                    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                    <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </ul>
            </div>
        </g:if>

        <div id="fh5co-product">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><g:message code="purchase" /></h2>
                            </div>
                            <div class="col-md-12">
                                <label><g:message code="purchase.amount"/>:</label>
                                <span>$${purchase.amount.amount}</span>
                            </div>
                            <div class="col-md-12">
                                <label><g:message code="purchase.date"/>:</label>
                                <span><g:formatDate format="dd-MM-yyyy HH:mm" date="${purchase.date}"/></span>
                            </div>
                            <div class="col-md-12">
                                <label><g:message code="user.label" default="Usuario"/>:</label>
                                <span>${purchase.user.name} ${purchase.user.lastName}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="credit-card-div">
                            <div class="panel panel-default">
                                <div class="panel-heading">

                                    <div class="row ">
                                        <div class="col-md-12">
                                            <input class="form-control" placeholder="Enter Card Number" type="text">
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <span class="help-block text-muted small-font"> Expiry</span>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <span class="help-block text-muted small-font"> Month</span>
                                            <input class="form-control" placeholder="MM" type="text">
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <span class="help-block text-muted small-font">  Year</span>
                                            <input class="form-control" placeholder="YY" type="text">
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <span class="help-block text-muted small-font">  CCV</span>
                                            <input class="form-control" placeholder="CCV" type="text">
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <asset:image src="cc-card.png" />
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row ">
                                        <div class="col-md-12 pad-adjust">

                                            <input class="form-control" placeholder="Name On The Card" type="text">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pad-adjust">
                                            <div class="checkbox">
                                                <label>
                                                    <input checked="" class="text-muted" type="checkbox"> Save details for fast payments <a href="#"> learn how ?</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                            <g:link class="btn btn-danger btn-outline btn-md" action="show" resource="${purchase}"><g:message code="default.cancel"/></g:link>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                            <g:link class="btn btn-warning btn-outline btn-md" action="validatePay" resource="${purchase}"><g:message code="purchase.pay"/></g:link>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- CREDIT CARD DIV END -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    </body>
</html>
