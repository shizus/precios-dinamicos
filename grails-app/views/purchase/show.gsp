<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'purchase.label', default: 'Purchase')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
    <div id="page">

        <g:if test="${user && user.isAdmin}">
            <div class="nav" role="navigation">
                <ul>
                    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                    <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </ul>
            </div>
        </g:if>

        <div id="fh5co-product">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><g:message code="purchase" /></h2>
                            </div>
                            <div class="col-md-12">
                                <label><g:message code="purchase.amount"/>:</label>
                                <span>$${purchase.amount.amount}</span>
                            </div>
                            <div class="col-md-12">
                                <label><g:message code="purchase.date"/>:</label>
                                <span><g:formatDate format="dd-MM-yyyy HH:mm" date="${purchase.date}"/></span>
                            </div>
                            <div class="col-md-12">
                                <label><g:message code="user.label" default="Usuario"/>:</label>
                                <span>${purchase.user.name} ${purchase.user.lastName}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <div id="show-purchase" class="content scaffold-show" role="main">
                            <table class='display'>
                                <tr>
                                    <th><g:message code="cart.image"/></th>
                                    <th><g:message code="cart.name"/></th>
                                    <th><g:message code="cart.description"/></th>
                                    <th><g:message code="cart.price"/></th>
                                </tr>
                                <g:each in="${purchase.products}" var="product">
                                    <tr>
                                        <td>
                                            <asset:image src="products/${product.id}.jpg" height="50" alt="${product.name}" title="${product.name}"/>
                                        </td>
                                        <td>${product.name}</td>
                                        <td>${product.description}</td>
                                        <td>${product.price.amount}</td>
                                    </tr>
                                </g:each>
                            </table>
                            <g:if test="${purchase.isPending()}">
                                <g:link class="btn btn-success btn-outline btn-md" action="pay" resource="${purchase}"><g:message code="purchase.pay"/></g:link>
                            </g:if>

    <g:if test="${user && user.isAdmin}">
                                <g:form resource="${this}" method="DELETE">
                                    <fieldset class="buttons">
                                        <g:link class="edit" action="edit" resource="${this.purchase}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                        <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                    </fieldset>
                                </g:form>
                            </g:if>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </body>
</html>
