package preciosdinamicos

class Money implements grails.validation.Validateable {
    String  currency
    Float amount

    Money() {
        this.currency = "ARS"
        this.amount = 0
    }

    Money(Float amount, String currency) {
        this.currency = currency
        this.amount = amount
    }

    def compareTo(float newAmount) {
        this.amount - newAmount
    }
}