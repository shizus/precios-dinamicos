package preciosdinamicos


class CurrentDayTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = 'preciosdinamicos'

    def springSecurityService
    def timeService

    def currentDay = {
        String output = timeService ? timeService.getCurrentDay() : "TimeService did not load"
        out << output
    }
}
