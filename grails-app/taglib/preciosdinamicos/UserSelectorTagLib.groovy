package preciosdinamicos

class UserSelectorTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = 'my'

    def springSecurityService

    def userSelector = { attrs ->
        def users = User.findAll()
        def activeUserId = session["user"]
        def activeUser = User.findById(activeUserId)
        if (activeUser) {
            attrs.value = activeUser.id
        }
        attrs.from = users
        attrs.optionKey = attrs.optionKey ?: 'id'
        attrs.optionValue = attrs.optionValue ?: 'name'
        attrs.onchange = "login(this.value)"
        attrs.noSelection = ["": "Iniciar sesión"]
        out << g.select(attrs)
    }
}
