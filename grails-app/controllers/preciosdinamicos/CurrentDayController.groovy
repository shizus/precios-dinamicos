package preciosdinamicos

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CurrentDayController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CurrentDay.list(params), model:[currentDayCount: CurrentDay.count()]
    }

    def show(CurrentDay currentDay) {
        respond currentDay
    }

    def create() {
        respond new CurrentDay(params)
    }

    @Transactional
    def save(CurrentDay currentDay) {
        if (currentDay == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (currentDay.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond currentDay.errors, view:'create'
            return
        }

        currentDay.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'currentDay.label', default: 'CurrentDay'), currentDay.id])
                redirect currentDay
            }
            '*' { respond currentDay, [status: CREATED] }
        }
    }

    def edit(CurrentDay currentDay) {
        respond currentDay
    }

    @Transactional
    def update(CurrentDay currentDay) {
        if (currentDay == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (currentDay.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond currentDay.errors, view:'edit'
            return
        }

        currentDay.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'currentDay.label', default: 'CurrentDay'), currentDay.id])
                redirect currentDay
            }
            '*'{ respond currentDay, [status: OK] }
        }
    }

    @Transactional
    def delete(CurrentDay currentDay) {

        if (currentDay == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        currentDay.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'currentDay.label', default: 'CurrentDay'), currentDay.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'currentDay.label', default: 'CurrentDay'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
