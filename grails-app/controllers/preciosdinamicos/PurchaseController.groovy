package preciosdinamicos

class PurchaseController {
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 20, 100)
        def user = getUserFromSession()
        List<Purchase> purchases = Purchase.findAllByUser(user)
        respond purchases, model: [purchaseCount: purchases.size(), max: params.max]
    }

    def show(Purchase purchase) {
        def user = User.findById(session.getAttribute("user"))
        respond purchase, model: [user: user]
    }

    def pay(Purchase purchase) {
        def user = User.findById(session.getAttribute("user"))
        respond purchase, model: [user: user]
    }

    def validatePay(Purchase purchase) {
        try {
            purchase.pay()
            flash.message = "Felicitaciones por su compra!"
            flash.class = "alert alert-success"
        } catch (IllegalAccessException ex) {
            flash.message = "Esta compra ya está pagada."
            flash.class = "alert alert-danger"
        }

        redirect(controller: 'purchase', action: 'show', id: purchase.getId())
    }


    def removeInvalidPurchases() {
        def user = getUserFromSession()
        Purchase.removeInvalidPurchases(user)
        flash.message = "Se han removido las compras inválidas"
        redirect(controller: 'purchase', action: 'index')
    }

    private User getUserFromSession() {
        def userId = session.getAttribute("user")
        User user = User.findById(userId)
        user
    }
}
