package preciosdinamicos

class PaymentController {
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {}

    def show(Payment payment) {
        respond payment
    }

    def pay() {
        flash.message = "La compra se ha realizado exitosamente"
        redirect getUserFromSession().getCart()
    }

    private User getUserFromSession() {
        def userId = session.getAttribute("user")
        User user = User.findById(userId)
        user
    }
}
