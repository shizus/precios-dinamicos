package preciosdinamicos

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller:"product",
            action:"index")
        "/login/$id"(controller:"login")
        "/admin"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
