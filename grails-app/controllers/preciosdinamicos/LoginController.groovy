package preciosdinamicos

class LoginController {

    def index() {
        def user = User.findById(params.id)
        session.setAttribute("user", user.getId())
        redirect(controller: 'product', action:'index')
    }
}
