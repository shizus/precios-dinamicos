package preciosdinamicos

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ProductController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 20, 100)
        List<Product> products
        if (params.category) {
            def category = ProductCategory.findByName(params.category)
            def stock = params.stock == 'true' ? 1 : 0
            products = category ? Product.findAllByCategoryAndStockGreaterThanEquals(category, stock) : Product.list(params)
        } else {
            products = Product.list(params)
        }
        def userId = session["user"]
        def user = User.findById(userId)
        if (user && !user.isAdmin) {
            products.each {
                product ->
                    product.calculateDinamicPrice(user)
            }
        }

        respond products, model: [productCount: Product.count(), max: params.max]
    }

    def show(Product product) {
        def userId = session.getAttribute("user")
        if (!userId) {
            flash.message = "Debe loguearse para poder ver productos"
            redirect(controller: 'product', action: 'index')
        } else {
            updateProductDinamically(product)
            product.bookVisit(User.findById(userId))
            def user = User.findById(userId)
            respond product, model: [user: user]
        }
    }

    def create() {
        respond new Product(params)
    }

    @Transactional
    def save(Product product) {
        if (product == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (product.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond product.errors, view: 'create'
            return
        }

        product.save()

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*' { respond product, [status: CREATED] }
        }
    }

    def edit(Product product) {
        respond product
    }

    @Transactional
    def update(Product product) {
        if (product == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (product.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond product.errors, view: 'edit'
            return
        }

        product.save()

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*' { respond product, [status: OK] }
        }
    }

    @Transactional
    def delete(Product product) {

        if (product == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        if (Cart.findAll().findAll { it.products.contains(product) }.isEmpty()) {
            ProductVisit.findByProduct(product)?.delete()
            product.delete()
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Transactional
    def addToCart(Product product) {
        try {
            User user = User.findById(session.getAttribute("user"))
            if (!user) {
                flash.message = "Debe loguearse para poder agregar productos"
                redirect(controller: 'product', action: 'index')
            } else {
                user.addToCart(product)
                redirect(controller: 'cart', action: 'show', id: user.getCart().getId())
            }
        } catch (StockException e) {
            flash.message = "${e.getMessage()}. Aquí tiene algunos productos relacionados en stock."
            redirect(controller: 'product', action: 'index', params: [category: product.getCategory().getName(), stock: true])
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'product.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    protected Product updateProductDinamically(Product product) {
        User user = User.findById(session["user"])
        if (user && !user.isAdmin) {
            product.calculateDinamicPrice(user)
        }
        product
    }
}
