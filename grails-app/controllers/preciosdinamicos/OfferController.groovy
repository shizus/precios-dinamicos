package preciosdinamicos

import grails.transaction.Transactional
import org.codehaus.groovy.runtime.StackTraceUtils

@Transactional(readOnly = true)
class OfferController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def show() {
        def userId = session.getAttribute("user")
        Offer offers = Offer.createOffers(User.findById(userId))
        render(view: "/offer/show.gsp", model: [offers: offers, offersCount: offers.getProducts().size()])
    }
}
