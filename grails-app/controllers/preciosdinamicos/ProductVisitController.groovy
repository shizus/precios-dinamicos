package preciosdinamicos

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProductVisitController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ProductVisit.list(params), model:[productVisitCount: ProductVisit.count()]
    }

    def show(ProductVisit productVisit) {
        respond productVisit
    }

    def create() {
        respond new ProductVisit(params)
    }

    @Transactional
    def save(ProductVisit productVisit) {
        if (productVisit == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (productVisit.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond productVisit.errors, view:'create'
            return
        }

        productVisit.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'productVisit.label', default: 'ProductVisit'), productVisit.id])
                redirect productVisit
            }
            '*' { respond productVisit, [status: CREATED] }
        }
    }

    def edit(ProductVisit productVisit) {
        respond productVisit
    }

    @Transactional
    def update(ProductVisit productVisit) {
        if (productVisit == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (productVisit.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond productVisit.errors, view:'edit'
            return
        }

        productVisit.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'productVisit.label', default: 'ProductVisit'), productVisit.id])
                redirect productVisit
            }
            '*'{ respond productVisit, [status: OK] }
        }
    }

    @Transactional
    def delete(ProductVisit productVisit) {

        if (productVisit == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        productVisit.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'productVisit.label', default: 'ProductVisit'), productVisit.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'productVisit.label', default: 'ProductVisit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
