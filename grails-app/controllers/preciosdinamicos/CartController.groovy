package preciosdinamicos

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional
class CartController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Cart.list(params), model: [cartCount: Cart.count()]
    }

    def show(Cart cart) {
        respond cart
    }

    def create() {
        respond new Cart(params)
    }

    def goToPurchases(){
        redirect(controller: 'purchase', action:'index')
    }

    def goToCart() {
        def userId = session.getAttribute("user")
        if (!userId) {
            flash.message = "Debe loguearse para poder agregar productos"
            redirect(controller: 'product', action: 'index')
        } else {
            User user = User.findById(userId)
            Cart cart = user.getCart()
            redirect cart
        }
    }

    def buy() {
        Cart cart = getCartData()
        if (!cart.products) {
            flash.message = "El carrito esta vacio"
            redirect cart
        } else {
            User currentUser = getUserFromSession()
            def purchase = cart.buy(currentUser)
            redirect purchase
        }
    }


    def clear() {
        Cart cart = getCartData()
        cart.deleteProducts()
        cart.save()
        flash.message = "Se ha vaciado el carrito"
        redirect cart
    }

    def remove(){
        Cart cart = getCartData()
        cart.removeProduct(params.id)
        flash.message = "Se ha removido el producto ${product.name}"
        redirect cart
    }

    private Cart getCartData() {
        User user = getUserFromSession()
        Cart cart = Cart.findById(user.getCartId())
        cart
    }

    private User getUserFromSession() {
        def userId = session.getAttribute("user")
        User user = User.findById(userId)
        user
    }

    @Transactional
    def save(Cart cart) {
        if (cart == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (cart.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond cart.errors, view: 'create'
            return
        }

        cart.save()

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'cart.label', default: 'Cart'), cart.id])
                redirect cart
            }
            '*' { respond cart, [status: CREATED] }
        }
    }

    def edit(Cart cart) {
        respond cart
    }

    @Transactional
    def update(Cart cart) {
        if (cart == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (cart.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond cart.errors, view: 'edit'
            return
        }

        cart.save() // acá había un update ?

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'cart.label', default: 'Cart'), cart.id])
                redirect cart
            }
            '*' { respond cart, [status: OK] }
        }
    }

    @Transactional
    def delete(Cart cart) {

        if (cart == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        cart.delete()

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'cart.label', default: 'Cart'), cart.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'cart.label', default: 'Cart'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
