package preciosdinamicos

class CleanPaymentsJob {
    static triggers = {
        cron name: 'myTrigger', cronExpression: "0 0 * * * ?"
    }

    def execute() {
        def users = User.findAll()
        users.each {
            Purchase.removeInvalidPurchases(it)
        }
    }
}
