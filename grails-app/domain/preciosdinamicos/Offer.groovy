package preciosdinamicos

import java.math.RoundingMode

class Offer {
    static final minBought = 2 // cantidad minima de veces comprada la combinacion para que se transforme en oferta
    static final discountPercentage = new BigDecimal(0.1).setScale(2, RoundingMode.HALF_UP)
    List<Product> products = new ArrayList<Product>()
    Money amount

    static embedded = ['amount']


    static Offer createOffers(User user) {
        List<Cart> carts = Cart.findAllByState(CartState.BOUGHT)
        // Obtengo un array con todos los productos comprados (incluyendo repetidos)
        String[] products = carts.collect{it.products}.flatten().collect{it.name}
        // filtro los productos a aquellos que fueron comprados mas de minBought veces
        def productsCount = products.flatten().groupBy{it}.collect{k,v->v.countBy{it}}.collectEntries{it}.findAll{it.value >= minBought}
        // ordeno por los que mas veces fueron comprados
        def productsSorted = productsCount.sort{it.value}
        // nos quedamos solo con dos combos
        String[] offers = productsSorted.take(2).keySet()
        Offer offer = new Offer()
        if (offers.size() == 0) return offer // si no se hallaron resultados devolvemos la oferta sin productos
        offer.products = offers.collect({Product.findByName(it)})
        offer.amount = new Money(offer.products.sum({it.calculateDinamicPrice(user)}), "ARS")
        offer.amount.setAmount(offer.amount.amount - offer.amount.amount * discountPercentage)
        offer
    }
}
