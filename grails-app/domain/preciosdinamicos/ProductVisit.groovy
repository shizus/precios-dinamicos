package preciosdinamicos


class ProductVisit {
    User user
    Product product
    Integer day
    Integer count

    ProductVisit(User user, Product product, Integer day, Integer count) {
        this.user = user
        this.product = product
        this.day = day
        this.count = count
    }

    void increaseVisit() {
        this.count = this.count ?: 0
        this.count++
        this.save()
    }
    static constraints = {
        user nullable: false
        product nullable: false
    }

    String toString() {
        "User: " + this.user +
                "\n Product: " + this.product +
                "\n day: " + this.day +
                "\n count: " + this.count
    }
}
