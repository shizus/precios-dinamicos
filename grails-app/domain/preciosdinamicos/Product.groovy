package preciosdinamicos

import preciosdinamicos.price_strategies.DiscountStrategy
import preciosdinamicos.price_strategies.PriceStrategy
import preciosdinamicos.price_strategies.SinStrategy
import preciosdinamicos.price_strategies.TrollStrategy

class Product {

    String name
    Money price
    String description
    ProductCategory category
    Integer stock
    String id
    def timeService

    static embedded = ['price']


    Product(String name, Money price, String description, ProductCategory category, Integer stock) {
        this.name = name
        this.price = price
        this.description = description
        this.category = category
        this.stock = stock
        this.id = id
    }


    static constraints = {
        name blank: false, nullable: false
        description blank: false, nullable: false
    }

    PriceStrategy selectStrategy(User user) {
        if (user.getIsVip()) {
            new DiscountStrategy()
        } else {
            if (this.price.compareTo(1000) > 0 ) {
                new TrollStrategy()
            } else {
                new SinStrategy()
            }
        }
    }

    float calculateDinamicPrice(User user) {
        PriceStrategy priceStrategy = selectStrategy(user)
        this.price.setAmount(priceStrategy.calculateDinamicPrice(user, this))
        this.price.amount
    }

    void bookVisit(User user) {
        ProductVisit productVisit = ProductVisit.findOrSaveByUserAndDayAndProduct(user, timeService.getCurrentDay(), this)
        productVisit.increaseVisit()
    }

    String toString() {
        "ID: " + this.getId() + " Nombre: " + this.name
    }
}
