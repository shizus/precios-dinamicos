package preciosdinamicos

class CurrentDay {

    Integer day

    CurrentDay(Integer day) {
        this.day = day
    }

    static constraints = {
        day nullable: false
    }
}
