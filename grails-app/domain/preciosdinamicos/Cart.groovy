package preciosdinamicos

class Cart {
    List<Product> products
    CartState state = CartState.PENDING

    def deleteProducts() {
        products.clear()
    }

    def buy(User user) {
        this.setState(CartState.BOUGHT)
        this.save()
        user.newCart()
        createPurchase(user, this.products)
    }

    def createPurchase(User user, List<Product> products) {
        def purchase = new Purchase(user, products)
        purchase.save()
        purchase
    }

    def removeProduct(def productId) {
        Product product = Product.findById(productId)
        products.remove(product)
        this.save()
    }

    Cart() {
        this.products = new ArrayList<Product>()
    }
    static constraints = {
    }
    static belongsTo = [user: User]
}
