package preciosdinamicos

class PurchaseItem {
    String code
    String name
    Money price
    String description
    static constraints = {
        name blank: false, nullable: false
        description blank: false, nullable: false
        code blank: false, nullable: false
    }

    static embedded = ['price']


    PurchaseItem(String code, String name, Money price, String description) {
        this.code = code.toString()
        this.name = name
        this.price = price
        this.description = description
    }
}
