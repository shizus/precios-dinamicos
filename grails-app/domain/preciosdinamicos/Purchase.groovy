package preciosdinamicos

import javax.transaction.Transactional

enum PurchaseStatus {
    PENDING,
    DONE
}

class Purchase {

    User user
    List<PurchaseItem> products
    Date date
    Money amount
    PurchaseStatus status

    static constraints = {
        
    }
    static mapping = {
        products cascade: 'all'
    }
    static embedded = ['amount']

    Purchase(User user, List<Product> products) {
        this.user = user
        float totalAmount = products?.sum { p -> p.calculateDinamicPrice(user) }
        this.amount = new Money(totalAmount, "ARS")
        this.products = new ArrayList<>()
        products.each {e -> this.products.add(new PurchaseItem(e.id,e.name,e.price,e.description))}
        this.date = new Date()
        status = PurchaseStatus.PENDING
    }

    def pay() {
        if (this.status == PurchaseStatus.PENDING) {
            this.status = PurchaseStatus.DONE
            Payment payment = new Payment(this, "VISA", this.amount)
            payment.save()
        } else {
            throw IllegalAccessException("No se puede pagar una compra ya pagada")
        }
    }

    def isPending() {
        PurchaseStatus.PENDING == this.status
    }

    def static removeInvalidPurchases(User user) {
        List<Purchase> purchases = findAllByUser(user)
        List<Purchase> purchasesToRemove = purchases.findAll { p -> p.date < new Date() - 15}
        removePurchases(purchasesToRemove)
    }

    @Transactional
    private static void removePurchases(List<Purchase> purchases) {
        purchases.each {
            p ->
                p.products.each {
                    prod ->
                        def product = Product.findById(prod.code)
                        product.stock++
                        product.save()
                }
        }
        purchases*.delete()
    }
}
