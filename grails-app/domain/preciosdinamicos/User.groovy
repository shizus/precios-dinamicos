package preciosdinamicos

import preciosdinamicos.StockException

class User {

    String name
    String lastName
    String address
    String email
    String password
    Cart cart
    Boolean isAdmin
    Boolean isVip

    User(String name, String lastName, String address, String email, String password) {
        this.name = name
        this.lastName = lastName
        this.address = address
        this.email = email
        this.password = password
        this.cart = new Cart()
        this.isAdmin = Boolean.FALSE
        this.isVip = Boolean.FALSE
    }

    User(String name, String lastName, String address, String email, String password, Boolean isAdmin) {
        this.name = name
        this.lastName = lastName
        this.address = address
        this.email = email
        this.password = password
        this.cart = new Cart()
        this.isAdmin = isAdmin
        this.isVip = Boolean.FALSE
    }

    User(String name, String lastName, String address, String email, String password, Boolean isAdmin, Boolean isVip) {
        this.name = name
        this.lastName = lastName
        this.address = address
        this.email = email
        this.password = password
        this.cart = new Cart()
        this.isAdmin = isAdmin
        this.isVip = isVip
    }

    static constraints = {
        name blank: false, nullable: false
        password password: true
        email email: true
    }

    void addToCart(Product product) {
        def stock = product.getStock();
        if (stock >= 1) {
            product.setStock(product.getStock()-1)
            this.getCart().getProducts().add(product)
        } else {
            throw new StockException("No hay más stock de este producto")
        }

    }

    void newCart() {
        this.cart = new Cart()
        this.save()
    }

    String toString() {
        "ID: " + this.id + " Name: " + this.name
    }
}
