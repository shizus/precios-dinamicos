package preciosdinamicos.price_strategies

import preciosdinamicos.Product
import preciosdinamicos.User

abstract class PriceStrategy {

    abstract float calculateDinamicPrice(User user, Product product)
}
