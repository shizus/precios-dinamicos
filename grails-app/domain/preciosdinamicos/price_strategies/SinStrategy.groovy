package preciosdinamicos.price_strategies


import preciosdinamicos.Product
import preciosdinamicos.ProductVisit
import preciosdinamicos.User



class SinStrategy extends PriceStrategy {

    def timeService

    float calculateDinamicPrice(User user, Product product) {
        // Al principio mientras más visita más sube, hasta que en un momento empieza a bajar

        def currentDay = timeService.getCurrentDay()
        def finalPrice = product.price.amount
        List productVisits = ProductVisit.findAllWhere(user: user, product: product)
        if (productVisits) {
            ProductVisit last = productVisits.last()
            if (last && currentDay == last.day) {
                float multiplier = Math.sin(productVisits.size() * Math.PI/ 6)
                multiplier = Math.round(multiplier * 10) / 10
                multiplier = Math.abs(multiplier) // siempre debe ser positivo
                float partialPrice = product.price.amount * (1 + multiplier * 0.2)
                finalPrice = Math.round(partialPrice * 100) / 100
            } else {
                finalPrice = last.product.price.amount
            }
        }
        finalPrice
    }

}
