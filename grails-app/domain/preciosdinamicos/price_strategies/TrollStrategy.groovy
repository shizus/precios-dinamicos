package preciosdinamicos.price_strategies


import preciosdinamicos.Product
import preciosdinamicos.ProductVisit
import preciosdinamicos.User


class TrollStrategy extends PriceStrategy {

    float calculateDinamicPrice(User user, Product product) {
        // Se realiza un aumento de a saltos de un 5 porciento cada 5 visitas. con 0 visitas se devuelve el price
        // minimo
        def visitsPerJump = 5
        def percentagePerJump = 0.05
        def maxIncrease = 0.25


        def finalPrice = product.price.amount
        List<ProductVisit> productVisits = ProductVisit.findAllWhere(user: user, product: product)
        float multiplier
        def visitsCount = productVisits.sum { x ->
            x.getCount()
        }
        if (visitsCount == null) return finalPrice
        multiplier = visitsCount / visitsPerJump
        multiplier = multiplier.trunc()
        multiplier = multiplier * percentagePerJump

        if (multiplier > maxIncrease) multiplier = maxIncrease;

        float partialPrice = product.price.amount * (1 + multiplier)
        Math.round(partialPrice * 100) / 100
    }

}
