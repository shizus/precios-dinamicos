package preciosdinamicos.price_strategies


import preciosdinamicos.Product
import preciosdinamicos.ProductVisit
import preciosdinamicos.User


class DiscountStrategy extends PriceStrategy {

    float calculateDinamicPrice(User user, Product product) {
        // Se realiza un descuento de 0.25 divido la cantidad de visitas totales del producto.
        List<ProductVisit> productVisits = ProductVisit.findAllWhere(product: product)
        float multiplier
        def visitsCount = productVisits.sum { x ->
            x.getCount()
        }
        if (visitsCount == null || visitsCount == 0) {
            multiplier = 0.25
        } else {
            multiplier = 0.25 / visitsCount
        }
        float partialPrice = product.price.amount * (1 + multiplier)
        Math.round(partialPrice * 100) / 100
    }

}
