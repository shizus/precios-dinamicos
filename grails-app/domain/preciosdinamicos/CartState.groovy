package preciosdinamicos

/**
 * Created by kunta on 06/11/17.
 */
enum CartState {
    PENDING, BOUGHT
}