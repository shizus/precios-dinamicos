package preciosdinamicos

class ProductCategory {
    String name

    ProductCategory(String name) {
        this.name = name
    }

    static constraints = {
        name blank: false, nullable: false
    }

    String toString() {
        this.name
    }
}
