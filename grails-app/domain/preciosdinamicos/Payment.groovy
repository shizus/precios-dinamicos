package preciosdinamicos

class Payment {
    Purchase purchase

    String paymentMethod
    Date date
    Money amount
    static constraints = {
    }

    static embedded = ['amount']


    Payment(Purchase purchase, String paymentMethod, Money amount) {
        this.purchase = purchase
        this.paymentMethod = paymentMethod
        this.amount = amount
        this.date = new Date()
    }

}
