package preciosdinamicos

import grails.transaction.Transactional

@Transactional
class TimeService {

    Integer getCurrentDay() {
        ((CurrentDay) CurrentDay.findAll().first()).getDay()
    }
}
