import preciosdinamicos.Cart
import preciosdinamicos.CurrentDay
import preciosdinamicos.Money
import preciosdinamicos.Product
import preciosdinamicos.ProductCategory
import preciosdinamicos.Purchase
import preciosdinamicos.User

class BootStrap {
    def init = { servletContext ->
        environments {
            development {
                if (ProductCategory.count() == 0) {
                    loadProductCategoryData()
                }
                if (Product.count() == 0) {
                    loadProductData()
                }
                if (User.count() == 0) {
                    loadUserData()
                }
                if (CurrentDay.count() == 0) {
                    loadCurrentDay()
                }
                if (Purchase.count() == 0) {
                    loadPurchases()
                }
            }
        }
    }

    def destroy = {
        ProductCategory.findAll().clear().save()
        Product.findaAll().clear().save()
        User.findAll().clear().save()
        Cart.findAll().clear().save()
        CurrentDay.clear().save()
    }

    def loadProductCategoryData() {
        new ProductCategory("Libros").save(flush: true)
        new ProductCategory("Tazas").save(flush: true)
//        new ProductCategory("Antigüedades").save(flush: true)
//        new ProductCategory("Arte y Artesanías").save(flush: true)
//        new ProductCategory("Bebés").save(flush: true)
//        new ProductCategory("Cámaras y Accesorios").save(flush: true)
//        new ProductCategory("Celulares y Teléfonos").save(flush: true)
//        new ProductCategory("Coleccionables y Hobbies").save(flush: true)
//        new ProductCategory("Computación").save(flush: true)
//        new ProductCategory("Consolas y Videojuegos").save(flush: true)
//        new ProductCategory("Delicatessen y Vinos").save(flush: true)
//        new ProductCategory("Deportes y Fitness").save(flush: true)
//        new ProductCategory("Electrodomésticos y Aires Ac.").save(flush: true)
//        new ProductCategory("Electrónica, Audio y Video").save(flush: true)
//        new ProductCategory("Entradas para Eventos").save(flush: true)
//        new ProductCategory("Hogar, Muebles y Jardín").save(flush: true)
//        new ProductCategory("Industrias y Oficinas").save(flush: true)
//        new ProductCategory("Instrumentos Musicales").save(flush: true)
//        new ProductCategory("Joyas y Relojes").save(flush: true)
//        new ProductCategory("Juegos y Juguetes").save(flush: true)
//        new ProductCategory("Libros, Revistas y Comics").save(flush: true)
//        new ProductCategory("Música, Películas y Series").save(flush: true)
//        new ProductCategory("Ropa y Accesorios").save(flush: true)
//        new ProductCategory("Salud y Belleza").save(flush: true)
//        new ProductCategory("Otras categorías").save(flush: true)
    }

    def loadProductData() {
        new Product("Grails in Action", new Money( 600, "ARS"), "Libro de Grails", ProductCategory.findByName("Libros"), 0).save(flush: true)
        new Product("Groovy and Grails Recipes", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Libros"), 80).save(flush: true)
        new Product("Practical Grails 3", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Libros"), 80).save(flush: true)
        new Product("Groovy in Action", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Libros"), 80).save(flush: true)
        new Product("Programming Grails", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Libros"), 80).save(flush: true)
        new Product("Grails 2", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Libros"), 80).save(flush: true)
        new Product("Css is Awesome", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Tazas"), 80).save(flush: true)
        new Product("Give me a break", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Tazas"), 80).save(flush: true)
        new Product("It's not a bug", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Tazas"), 80).save(flush: true)
        new Product("Programmers don't byte", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Tazas"), 80).save(flush: true)
        new Product("My code is mistake free", new Money( 419, "ARS"), "Libro de Grails", ProductCategory.findByName("Tazas"), 80).save(flush: true)
    }

    def loadUserData() {
        new User("Daniel", "Shlufman", "San martin 481", "danielshlufman@gmail.com", "123456", Boolean.FALSE, Boolean.TRUE).save(flush: true)
        new User("Leandro", "Rinaldi", "San martin 481", "rinaldi.leandro@gmail.com", "123456").save(flush: true)
        new User("Gabriel", "La Torre", "San martin 481", "latorregab@gmail.com", "123456").save(flush: true)
        new User("Admin", "Admin", "San martin 481", "admin@preciosdinamicos007.co", "123456", Boolean.TRUE).save(flush: true)
    }

    def loadCurrentDay() {
        new CurrentDay(1).save(flush: true)
    }

    def loadPurchases() {
        def user = User.findByName("Leandro")
        def products = new ArrayList()
        products.add(Product.findByName("Grails in Action"))
        def purchase = new Purchase(user, products)
        purchase.setDate(new Date() - 16)
        purchase.save(flush: true)
    }
}
